% !TEX root = ../FnTIR2021-Rec.tex

%\section{Future directions and new perspectives}
\chapter{Future Directions and New Perspectives}
\label{sec:futuredirections}
In this section, future directions and new perspectives of applying \acp{VAE} in recommender systems are given, to encourage further research into using \acp{VAE} to solve problems in recommender systems. Specifically, we first point out future directions based on the trends reflected by the taxonomy displayed in Figure~\ref{fig:taxonomy}, and possible expansions based on the characteristics of \acp{VAE}. Additionally, we provide novel perspectives on applying \acp{VAE} in explainable recommender systems.

\section{Future directions based on the taxonomy}
Nearly all of the collaborative filtering methods in Section~\ref{sec:mainapproaches}  handle implicit feedback-based recommendation but only some (e.g.,~\citep{shen2019deep,ma2018partial,wu2021a}) deal with explicit feedback scenarios. 
They mainly focus on matrix data with binary values instead of the ratings, e.g., from 0 to 5. Although~\citet{iqbal2019style} also mentions explicit feedback data, they focus on implicit feedback-based recommendation. 
\citet{ma2018partial} utilize a \ac{VAE} to directly generate rating data, and use \ac{RMSE} to evaluate the performance on the rating prediction task. The experimental results show that there is room to improve the performance. To sum up, current \ac{VAE}-based recommendation methods mainly focus on implicit feedback, whereas explicit feedback is lack of exploration~\citep{younas2021deep}.
% which encourages us to explore the application of \acp{VAE} in explicit feedback scenarios.
% we can see from TABLE \ref{tab:ImplicitExplicit} that,

%\begin{table}
%    \caption{Publications in section \ref{sec:mainapproaches} based on different recommendation scenarios, i.e., implicit feedback or explicit feedback.}
%    \centering
%    \label{tab:ImplicitExplicit}
%    \setlength{\tabcolsep}{2mm}{
%    \begin{tabular}{|c|c|}
%        \hline
%        Scenarios & Publications \\
%        \hline
%        Implicit Feedback & \tabincell{c}{~\citep{lee2017augmented,liang2018variational,chen2018collective,gupta2018hybrid,cui2018variational,iqbal2019style},
%        ~\citep{polato2020conditioned,pang2019novel,karamanolakis2018item},\\~\citep{kim2019enhancing,shenbin2020recvae,wu2019one},
%        ~\citep{li2017collaborative,bai2019collaborative,he2019collaborative,xiao2019bayesian,wang2020personalized,xiao2019variational},\\
%        ~\citep{xiao2019neural,xiao2019neural2,deng2019collaborative,xiao2018neural,deng2019neural},
%        ~\citep{chen2020local,yu2019vaegan,liu2020deep,elahi2019variational},\\~\citep{chen2020fast,wang2018variational,zhong2020session},
%        ~\citep{sachdeva2019sequential,rohde2019latent,xiao2019hierarchical,song2019coupled}} \\
%        \hline
%        Explicit Feedback & ~\citep{ma2018partial,shen2019deep,jin2020leveraging,zhang2019deepvariational} \\
%        \hline
%    \end{tabular}
%    }
%\end{table}

In a dynamic setting, \acp{VAE} are used to model sequential data, or to optimize the model with sequential data. 
Methods that model sequential data usually replace the encoder and decoder with a \ac{GRU}, outperforming other methods without applying a \ac{VAE} by a moderate margin. Methods that utilize optimization strategies of \acp{VAE} to optimize the model, mainly design a deep Bayesian model. With the aid of a \ac{VAE} for efficient optimization, one can freely design models of dynamic methods. Modeling the temporal dependencies among items can help unveil the user's additional short-term preferences towards items, which seems more reasonable than only considering the long-term preferences of users. Experiments with these kinds of methods demonstrate that they outperform other state-of-the-art static methods. However, according to the prior work mentioned above, there is relatively little work on dynamic methods with \acp{VAE}. Indeed, more attention should be devoted the dynamic \ac{VAE}-based recommendation methods.

\section{Possible expansions based on the characteristics of \acp{VAE}}
\paragraph{Expansions of encoding capability.} 
In real world scenarios, items and users are usually related to different types of information available on the web. Thus, in future work, we can consider encoding diverse types data, other than textual data in \ac{VAE}-based recommendation models, e.g., images. How to take advantage of the encoding capability to encode heterogeneous data, and fuse them seamlessly either in observed space or latent space using a \ac{VAE} so as to improve the recommendation performance, is a challenging but promising direction. In addition, inspired by the fact that item-based collaborative filtering~\citep{sarwar2001item} outperforms user-based collaborative filtering, we suggest to encode the item-centric interaction data to improve the recommendation performance in future works.

\paragraph{Expansions of a generative nature.} 
The generative nature of a \ac{VAE} provides a natural way for extending current recommender systems. In addition to generating interaction data, in future work, the generative nature could help to generate personalized explanations of recommendations in the form of text for each user, increasing trust in the recommender system.

\paragraph{Expansions of a Bayesian nature.} 
One can explore more dependencies between different variables, since the architecture of the graphical model will influence the recommendation performance as stated by~\citet{lee2017augmented}. We suggest to consider adding more latent variables to the graphical model, to improve the recommendation performance. Prior work~\citep{maaloe2016auxiliary,ranganath2016hierarchical,salimans2015markov,wu2021a} has added more variables to improve the model. After designing the causal structure of the model, one should try to implement it; finding the most appropriate implementation for each type of graphical model is an exciting research direction.

\paragraph{Expansions of a flexible internal structure.} 
Although the use of neural networks for approximating the intractable posterior of latent variables has seen great improvements, in reducing the gap between the true posterior distribution and the latent distribution, there is always work to do. A powerful encoder can improve the quality of the learned representations, which is crucial for improving the recommendation performance. Current work that utilizes \acp{VAE} to directly learn interaction data usually uses a shallow neural network to model the interaction data, which can be high-dimensional. Thus one intuitive way forward is to deepen the encoder neural network, so as to ensure expressivity of the learned latent representation. Some work~\citep[e.g.,][]{shenbin2020recvae,zhong2020session,wu2021a} has explored this expansion. To alleviate the so-called ``posterior collapse'' problem and learn better latent representations, some methods listed in Section~\ref{subsubsec:staticgenerate} propose to use more reasonable priors. One can also resort to other priors such as autoregressive priors~\citep{chen2016variational}, Gaussian mixture priors~\citep{dilokthanakul2016deep}, a nested Chinese Restaurant Process prior~\citep{goyal2017nonparametric}, a stick-breaking prior~\citep{nalisnick2016stick}, hierarchical priors~\citep{klushyn2019learning}, or the prior aggregated by posteriors~\citep{molchanov2018doubly} like that in~\citep{kim2019enhancing}. We suggest to add more user-specific information into the priors, to make the learned latent distributions of different users diverse.

%\textbf{Expansions of ELBO} From the analysis of ELBO above, one future direction of reformulating ELBO is to explore more model architectures which can be theoretically proven by deriving the ELBO. One can also purely reformulate the ELBO mathematically so as to find more possible and improved recommender systems models.

